<?php

namespace Helium\FacadeManager\Tests\Base;

use Exception;
use Helium\FacadeManager\EngineContract;
use Helium\FacadeManager\Exceptions\InvalidEngineException;
use Helium\FacadeManager\Exceptions\UnknownEngineException;
use Helium\FacadeManager\FacadeManager;
use Orchestra\Testbench\TestCase;

abstract class FacadeManagerPackageTest extends TestCase
{
	protected abstract function getInstance(): FacadeManager;

	protected abstract function getNewEngine(): EngineContract;

	protected abstract function getNewEngine2(): EngineContract;

	public abstract function testPassthroughReturnsExpected();

	public function testExtendReturnsSelf()
	{
		$manager = $this->getInstance();

		$this->assertEquals(
			$manager,
			$manager->extend('fake2', function() {
				return $this->getNewEngine2();
			})
		);
	}

	public function testDefaultEngine()
	{
		$manager = $this->getInstance();

		$this->assertInstanceOf(get_class($this->getNewEngine()), $manager->engine());
	}

	public function testSpecificEngine()
	{
		$manager = $this->getInstance();

		$this->assertEquals(
			$manager,
			$manager->extend('fake2', function() {
				return $this->getNewEngine2();
			})
		);

		$this->assertInstanceOf(
			get_class($this->getNewEngine2()),
			$manager->engine('fake2')
		);
	}

	public function testUnknownEngineThrowsException()
	{
		$manager = $this->getInstance();

		try
		{
			$engine = $manager->engine('unknownEngine');

			$this->assertTrue(false);
		}
		catch (Exception $e)
		{
			$this->assertInstanceOf(UnknownEngineException::class, $e);
			$this->assertStringContainsString('unknownEngine', $e->getMessage());
		}
	}

	public function testInvalidEngineThrowsException()
	{
		$manager = $this->getInstance();
		$manager->extend('fake3', function() {
			return 'notAnEngine';
		});

		try
		{
			$engine = $manager->engine('fake3');

			$this->assertTrue(false);
		}
		catch (Exception $e)
		{
			$this->assertInstanceOf(InvalidEngineException::class, $e);
			$this->assertStringContainsString('fake3', $e->getMessage());
			$this->assertStringContainsString('string', $e->getMessage());
			$this->assertStringContainsString(
				$manager->getEngineContract(),
				$e->getMessage()
			);
		}
	}
}