<?php

namespace Helium\FacadeManager\Tests\Fakes;

class FakeEngine implements FakeFacadeEngineContract
{
	public function chainMethod(): FakeFacadeEngineContract
	{
		return $this;
	}

	public function returnArray(): array
	{
		return [];
	}
}