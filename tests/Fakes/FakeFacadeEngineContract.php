<?php

namespace Helium\FacadeManager\Tests\Fakes;

use Helium\FacadeManager\EngineContract;

interface FakeFacadeEngineContract extends EngineContract
{
	public function chainMethod(): FakeFacadeEngineContract;

	public function returnArray(): array;
}