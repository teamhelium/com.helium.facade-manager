<?php

namespace Helium\FacadeManager\Tests\Fakes;

class Fake2Engine implements FakeFacadeEngineContract
{
	public function chainMethod(): FakeFacadeEngineContract
	{
		return $this;
	}

	public function returnArray(): array
	{
		return [];
	}
}