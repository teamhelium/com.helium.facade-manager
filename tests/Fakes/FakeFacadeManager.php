<?php

namespace Helium\FacadeManager\Tests\Fakes;

use Helium\FacadeManager\FacadeManager;

class FakeFacadeManager extends FacadeManager
{
	public function getEngineContract(): string
	{
		return FakeFacadeEngineContract::class;
	}
}