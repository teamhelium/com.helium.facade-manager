<?php

namespace Helium\FacadeManager\Tests;

use Helium\FacadeManager\EngineContract;
use Helium\FacadeManager\FacadeManager;
use Helium\FacadeManager\Tests\Base\FacadeManagerPackageTest;
use Helium\FacadeManager\Tests\Fakes\Fake2Engine;
use Helium\FacadeManager\Tests\Fakes\FakeEngine;
use Helium\FacadeManager\Tests\Fakes\FakeFacadeManager;

class FacadeManagerTest extends FacadeManagerPackageTest
{
	protected function getInstance(): FacadeManager
	{
		$manager = new FakeFacadeManager('fake');

		$manager->extend('fake', function() {
			return $this->getNewEngine();
		});

		return $manager;
	}

	protected function getNewEngine(): EngineContract
	{
		return new FakeEngine();
	}

	protected function getNewEngine2(): EngineContract
	{
		return new Fake2Engine();
	}

	public function testPassthroughReturnsExpected()
	{
		$manager = $this->getInstance();

		$this->assertEquals(
			$manager,
			$manager->chainMethod()
		);

		$this->assertIsArray($manager->returnArray());
	}
}