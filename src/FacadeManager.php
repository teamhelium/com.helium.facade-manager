<?php

namespace Helium\FacadeManager;

use Closure;
use Helium\FacadeManager\Exceptions\InvalidEngineException;
use Helium\FacadeManager\Exceptions\UnknownEngineException;

abstract class FacadeManager
{
	//region Base
	protected $defaultEngine;
	protected $builders = [];
	protected $engines = [];

	/**
	 * @description Construct HeliumFacadeManager subclass
	 * @param string $defaultEngine
	 */
	public function __construct(string $defaultEngine)
	{
		$this->defaultEngine = $defaultEngine;
	}

	/**
	 * @description Get the contract class or interface which all registered
	 * engines must implement
	 * @return string
	 */
	public abstract function getEngineContract(): string;
	//endregion

	//region Engine
	/**
	 * @description Get the specified or default engine for making requests
	 * @param string|null $key
	 * @return mixed
	 * @throws UnknownEngineException
	 */
	public function engine(string $key = null)
	{
		$key = $key ?? $this->defaultEngine;

		if (!isset($this->engines[$key]))
		{
			if (!isset($this->builders[$key]))
			{
				throw new UnknownEngineException(static::class, $key);
			}

			$callback = $this->builders[$key];
			$engine = $callback();

			if (!is_a($engine, $this->getEngineContract()))
			{
				$type = gettype($engine);

				$class = ($type == 'object') ? get_class($engine) : $type;

				throw new InvalidEngineException(
					$key,
					$this->getEngineContract(),
					$class);
			}

			$this->engines[$key] = $engine;
		}

		return $this->engines[$key];
	}

	/**
	 * @description Forward all method calls on the manager to the default engine
	 * @param $method
	 * @param $parameters
	 * @return $this
	 * @throws UnknownEngineException
	 */
	public function __call($method, $parameters)
	{
		$returnValue = $this->engine()->$method(...$parameters);

		if ($returnValue == $this->engine())
		{
			return $this;
		}

		return $returnValue;
	}
	//endregion

	//region Contract

	/**
	 * @description Register an engine implementation with the manager
	 * @param string $key
	 * @param Closure $callback
	 * @return static
	 */
	public function extend(string $key, Closure $callback): FacadeManager
	{
		$this->builders[$key] = $callback;

		return $this;
	}
	//endregion
}